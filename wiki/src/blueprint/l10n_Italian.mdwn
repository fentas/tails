[[!toc levels=2]]


# Definizioni 

Warning page = pagina degli avvertimenti/avvisi

Persistence = persistente https://it.wikipedia.org/wiki/Persistenza_%28informatica%29 

Sensitive = sensibile o riservato

[[Domande aperte sui termini]]

# Info

Qualche informazione sui file po:

<http://tp.linux.it/guida-po/index.html>

Per iniziare una nuova traduzione, fai copia incolla di un file già esistenete, 
tipo: about.fr.po, e lo rinomini about.pot
A quel punto lo apri con poedit e lui ti chiederà come nominarlo iniziando per una nuova lingua e tu gli dici: about.it
a quel punto inizi a tradurre :)

# Aiuto con GIT



##mini guida GIT

https://rogerdudler.github.io/git-guide/index.it.html

## Repository GIT

E' qui: https://git.lattuga.net/transitails/italian

        git clone http://git.lattuga.net/transitails/italian.git transitails

	git remote add origin http://git.lattuga.net/transitails/italian

	 
Per essere abilitati in scrittura, iscrivetevi a git.lattuga.net e chiedete in lista di accettarvi l'utente.

## Git comandi quotidiani

Tutte le righe che iniziano con $ sono da digitare nel terminale, a volte sotto c'è la risposta del terminale, oppure niente. In generale nei sistemi unix-like se il terminale dopo aver dato un comando non vi risponde niente, vuol dire che tutto è andato bene.
Il pulsante TAB è vostro amico per completare tutti i percorsi dei file e soprattutto quando usate git add. Le frecce su e giù della tastiera vi danno gli ultimi comandi che avete lanciato, così andate velocissim*. Tutto quello che inizia con [hagfsa] va sostituito con il nome che fa al caso vostro. Se vi trovate ad un certo punto intrappolati nel terminale e c'è ESC in fondo, niente paura, è vim, quindi fate: ESC : q
E lui vi fa uscire.

Sincronizziamoci al repository remoto, prendendo i file che ci mancano:
 
    $  git pull 

Ora controlliamo in che branch siamo
 
    $  git branch -a

Se non siamo su quella giusta facciamo 

    $git checkout [nome branch] 

Quindi iniziamo ad editare i file in locale usando POEDIT
    
Per aggiungere allo stadio "stage" i file modificati.

    $ git add [NAMEFILE]

Per sapere cosa sta per essere committato e cosa no:
   
    $ git status

Modifiche fatte localmente, già messe in "stage", che volete passare come "definitive" nel vostro repo locale:


    $ git commit -m "DESCRIZIONE DELLE MODIFICHE FATTE"

Se siete sicuri che le modifiche che avete fatto vanno sul repository remoto, spingetele là:

    $ git push origin [nome dalla branch o master]

Se non sapete l'identità con cui è configurato git, fate un controllo prima di mandare le cose in remoto:


    $ git config -l

tIn caso di dubbi, vedete un po il vostro status:

    $ git status


Verificate che il commit ci sia

     $git log origin [nome dalla branch o master]

## Alcuni utili alias

    git config --global alias.graph 'log --graph --oneline --all --decorate=short'
    git config --global alias.lg 'log --oneline --graph --decorate=short'

L'alias `graph` mostra un "grafo" della situazione corrente, in modo da riconoscere come sono disposte le varie branch e dove siete voi.
L'alias `lg` e' simile, ma non mostra TUTTE le branch possibili e immaginabili. Di default mostra solo lo stato corrente (tipo git log, ma piu' stringato). Se fatte tipo `git lg master mia-branch` potete vedere come sta messa la vostra branch rispetto al master. E' in avanti? indietro? ecc.

# Lavoro da affrontare

Attingere nuove pagine da tradurre dando precedenza a queste:

<https://tails.boum.org/contribute/l10n_tricks/core_po_files.txt>


#Branch

Le branch sono utili ma devono esistere il più breve tempo possibile. Servono per avere i file non revisionati fuori dal master. Chi revisiona può direttamente pushare nel master.


Voglio creare la nuova branch locale italia_about

    $ git branch italian_about

poi controllo che ci sia
 
    $ git branch

mi risponde la lista delle branch esistenti.

Allora vado a lavorarci sopra.

    $ git checkout italian_about

    $ git branch

mi fa rivedere le branch

Se voglio anche le branch remote faccio 

    $git branch -a

Ora modifico i vari file, faccio ADD, poi COMMIT e poi PUSH:

    $ git push [remote-name] [branch-name]

Dove [remote-name] è comunemente "origin", dicono, ma potrebbe essere diverso, dipende come avete impostato i vostri remote. Controllate con 

     $git remote -v

#Generare una copia del wiki in locale

Il sito web https://tails.boum.org/ è costruito usando Ikiwiki dal codice sorgente che è disponibile nel nostro repository Git, così come il resto del codice di Tails.

Tu puoi costruire la tua copia locale del sito sul tuo pc. La generazione del sito web consiste in una collezione di pagine HTML salvate sul tuo sistema operativo, che si possono aprire con un comunissimo browser anche se sei offline. Fare questo è molto utile per chi scrive la documentazione e i traduttori che così vedono applicate le loro modifiche prima di metterle sul sito web in produzione.

Genera il wiki in locale su TAILS

Crea e configura la partizione resistente attivando le seguenti funzionalità (Applicazioni>Tails>Configure persistent volume):
        Dati personali
        Pacchetti APT
        Liste APT

    Riavvia Tails, abilita la persistenza e le configurazioni avanzate, imposta una password di root.

    Aggiorna la lista dei pacchetti disponibili, scrivendo in un terminale:

    sudo apt-get update

Installa i seguenti pacchetti:

    sudo apt-get install libyaml-perl libyaml-libyaml-perl po4a \
     perlmagick libyaml-syck-perl ikiwiki

    Clona il nostro repository Git in una cartella della partizione persistente:

    cd ~/Persistent/
    git clone https://git-tails.immerda.ch/l10n-italian/tails/ mytails

    Il codice sorgente del sito internet si trova nella cartella wiki/src/ .

    Per accelerare la compilazione, puoi disabilitare alcune lingue nei parametri po_slave_languages delle configurazioni nei file ikiwiki.setup.
E poi lanciare ikiwiki --changesetup ikiwiki.setu

    Ora puoi visitare questo indirizzo locale dal tuo browser per vedere il sito web generato:

    file:///home/amnesia/Persistent/Tor Browser/tails/index.en.html
    Genera il sito web:

    cd mytails
    ./build-website --set destdir="/home/amnesia/Persistent/outtails" "$@"
___

# Come configurare il workflow git sul vostro pc VECCHIA VERSIONE


1) Creata una cartella con TRADUZIONI:

    $ mkdir TRADUZIONI

2) Entrata nella cartella:

    $ cd TRADUZIONI

3) Clonato i file per il mio uso locale nella cartella mytails (indicata
in fondo al comando):

    $ git clone  https://git-tails.immerda.ch/l10n-italian/tails/ mytails
    Cloning into 'mytails'...
    remote: Counting objects: 184366, done.
    remote: Compressing objects: 100% (48007/48007), done.
    remote: Total 184366 (delta 121678), reused 184279 (delta 121629)
    Ricezione degli oggetti: 100% (184366/184366), 49.13 MiB | 892.00 KiB/s,
    done.
    Risoluzione dei delta: 100% (121678/121678), done.
    Checking connectivity... fatto.

4)Entro nell cartella mytails e controllo che mi dice git:

    $ git status
    Sul branch master
    Your branch is up-to-date with 'origin/master'.
    nothing to commit, working directory clean

5)Aggiungo il repository remoto:

    $ git remote add origin tails@git.tails.boum.org:tails

6)Controllo che funzioni:

    $ git remote -v
    origin 	  tails@git.tails.boum.org:tails (fetch)
    origin        tails@git.tails.boum.org:tails (push)
    l10n-italian	  https://git-tails.immerda.ch/l10n-italian/tails/ (fetch)
    l10n-italian	  https://git-tails.immerda.ch/l10n-italian/tails/ (push)

7)Modifico dei file, aggiungo traduzioni, etc.. poi torno al terminale.

8)Aggiungo due file che ho creato e modificato:

    $ git add wiki/src/doc/about/license.it.po
    $ git add wiki/src/doc/about/requirements.it.po
    $ git status
    Sul branch master
    Your branch is up-to-date with 'origin/master'.
    Changes to be committed:
      (use "git reset HEAD <file>..." to unstage)
    
       new file:   wiki/src/doc/about/license.it.po
       new file:   wiki/src/doc/about/requirements.it.po

9) Faccio il commit locale

    $ git commit -m "primi file tradotti"
    [master c149e1b] primi file tradotti
     Committer: cri <cri@localhost.lan>
    Il tuo nome e l'indirizzo email sono stati configurati automaticamente usando
    il tuo nome utente ed il nome host. Per favore, verifica che siano esatti.
    È possibile eliminare questo messaggio impostandoli esplicitamente:
    
        git config --global user.name "Tuo Nome"
        git config --global user.email tu@esempio.com

    Dopo questa operazione, puoi ripristinare l'identità usata in questo commit con:
   
        git commit --amend --reset-author
    
     2 files changed, 203 insertions(+)
     create mode 100644 wiki/src/doc/about/license.it.po
     create mode 100644 wiki/src/doc/about/requirements.it.po

10)Configuro la mia identità (opzionale):

    $ git config --global user.name "Tails Developer"
    $ git config --global user.email "developer@blablabla.net"

11)Genero la chiave ssh, la invio agli sviluppatori TAILS(il file.pub) e l'associo per essere autenticato sul server:
    
    ssh-keygen -t rsa -b 4096 -C "ignifugo@blablabla.net"

Ti chiederà il nome con cui genererà i due file della chiave, quello pubblico e quello segreto. QUindi ti cheide una passwor, due volte; i caratteri non si vedono quando li digiti.Finito

Ora configuro la comunicazione ssh ad usare la mia chiave segreta ed invio quella pubblica agli sviluppatori di Tails per poter così scrivere nel repository condiviso.

    $ ssh-add /home/cri/ignissh
    Enter passphrase for /home/cri/ignissh:
    Identity added: /home/cri/ignissh (/home/cri/ignissh)
 
12) Metto i file sul server:

    $ git push l10n-italian master
    The authenticity of host 'git.tails.boum.org (77.109.139.10)' can't be established.
    RSA key fingerprint is ed:1b:5b:45:e4:9c:d6:8f:55:f3:5f:b7:44:30:42:17.
    Are you sure you want to continue connecting (yes/no)? yes
    Warning: Permanently added 'git.tails.boum.org,77.109.139.10' (RSA) to the list of known hosts.
    Counting objects: 8, done.
    Delta compression using up to 2 threads.
    Compressing objects: 100% (8/8), done.
    Writing objects: 100% (8/8), 4.04 KiB | 0 bytes/s, done.
    Total 8 (delta 4), reused 0 (delta 0)
    To tails@git.tails.boum.org:l10n-italian/tails
    42f6936..c149e1b  master -> master

13) Controllo che ci sia, sul terminale:

    $git log

Oppure in grafica dal browser:

<https://git-tails.immerda.ch/l10n-italian/tails/>


# Workflow

Come procediamo per fare varie cose?

## Pacchi

In breve:

1. creazione pacchi
2. assegnazione: ogni pacco ha un traduttore
3. per ogni pacco:
   * traduzione!
   * revisione (non chiaro: quando si sceglie chi revisiona?)
   * merge dentro master
 

Ogni tanto (quando?) si fanno dei pacchi di traduzioni. Un pacco e' un insieme di filename che vanno tradotti. Ogni pacco viene tradotto da una persona.

Supponiamo di tradurre la divina commedia, e di fare 3 pacchi: Inferno, Purgatorio e Paradiso. Petrarca si accolla di tradurre l'Inferno, e lo fa in una branch che chiama, appunto "inferno". traduce tutti i file  `wiki/src/inferno/*.po`, quindi fa `git commit -m "inferno tradotto"`.

A questo punto qualcuno deve revisionare la branch. Si fa avanti Laura. Laura fa `git fetch origin && git checkout origin/inferno`. Prima guarda le differenze introdotte da Petrarca, con `git log -p origin/inferno`.
Gli sembrano ok, ma per controllare che si vedano anche bene nel wiki lo builda con `./build-website` e poi lo guarda da firefox. Nota che Petrarca, sbadato, ha fatto alcuni piccoli errori nel markup del wiki che "rompono" delle immagini. Petrarca aveva tradotto la riga 
    [[!img \"caronte.png`"]]
con
    [[img \"caronte.png\"`]]

Laura corregge e fa commit sulla stessa branch `git commit wiki/src/inferno/terzocanto.po -m "fix immagine caronte" && git push origin inferno`.
Non trova altri errori, quindi fa

    git checkout master
    git merge inferno

Potrebbe subito fare `git push origin master` ma, per non sbagliare, fa un diff:

    git diff origin/master..master

E controlla se tutto torna. Ci sono conflitti? Quel cambiamento al `!img` c'e' ancora? bene, ora possiamo fare

    git push origin master

## Call for translations

Prima di una release, in lista tails-l10n arriva un'email "Call for translations". Come si procede a quel punto?
