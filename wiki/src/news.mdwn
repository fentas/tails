[[!meta title="News"]]

- Subscribe to the [[amnesia-news mailing
  list|about/contact#amnesia-news]] to receive the same news
  by email:
  <form method="POST" action="https://mailman.boum.org/subscribe/amnesia-news">
          <input class="text" name="email" value=""/>
          <input class="button" type="submit" value="Subscribe"/>
  </form>

- Follow us on Twitter [@Tails_live](https://twitter.com/tails_live).

[[!inline pages="page(news/*) and !news/*/* and !news/discussion and (currentlang() or news/report_2* or news/version_0* or news/test_0* or news/test_*-rc?)" show="10"]]
[[!inline pages="page(news/*) and !news/*/* and !news/discussion and (currentlang() or news/report_2* or news/version_0* or news/test_0* or news/test_*-rc?) and tagged(announce)"
show="10" feeds="yes" feedonly="yes" feedfile="emails"]]
